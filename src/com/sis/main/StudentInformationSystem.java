package com.sis.main;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by [] on 3/17/2017.
 * Student Information System
 * Features
 * - Determine the number of students per year level, per course, per college and per gender
 * - Sort information according by Name, Year Level, Course, College, Student Number
 * - All prompted by user choice . Display all the possible results
 */
public class StudentInformationSystem {

    enum YearLevel {
        NONE,
        FIRST,
        SECOND,
        THIRD,
        FOURTH
    }

    enum Course {
        BSIT,
        BSCS
    }

    enum College {
        CCS,
        CPE,
        COE
    }

    static class Student {
        public String number;
        public String name;
        /*
            m or f
         */
        public char gender;
        public YearLevel yearLevel = YearLevel.NONE;
        public Course course;
        public College college;

        public Student(String number, String name, char gender, YearLevel yearLevel, Course course, College college) {
            this.number = number;
            this.name = name;
            this.gender = gender;
            this.yearLevel = yearLevel;
            this.course = course;
            this.college = college;
        }

        public static Comparator<Student> COMPARE_BY_NAME = new Comparator<Student>() {
            @Override
            public int compare(Student a, Student b) {
                return a.name.compareTo(b.name);
            }
        };

        public static Comparator<Student> COMPARE_BY_YEARLEVEL = new Comparator<Student>() {
            @Override
            public int compare(Student a, Student b) {
                return Integer.signum(a.yearLevel.ordinal()-b.yearLevel.ordinal());
            }
        };

        public static Comparator<Student> COMPARE_BY_COURSE = new Comparator<Student>() {
            @Override
            public int compare(Student a, Student b) {
                return a.course.compareTo(b.course);
            }
        };

        public static Comparator<Student> COMPARE_BY_COLLEGE = new Comparator<Student>() {
            @Override
            public int compare(Student a, Student b) {
                return a.college.compareTo(b.college);
            }
        };

        public static Comparator<Student> COMPARE_BY_NUMBER = new Comparator<Student>() {
            @Override
            public int compare(Student a, Student b) {
                return a.number.compareTo(b.number);
            }
        };

        public static Comparator<Student> COMPARE_BY_GENDER = new Comparator<Student>() {
            @Override
            public int compare(Student a, Student b) {
                return new Character(a.gender).compareTo(new Character(b.gender));
            }
        };
    }


    static List<Student> students;

    static void createTestData() {
        students = new ArrayList<Student>(Arrays.asList(
                new Student("2017-3000", "Ron", 'M',YearLevel.FIRST, Course.BSIT, College.CCS),
                new Student("2017-2000", "Aiza", 'F',YearLevel.FIRST, Course.BSCS, College.CCS),
                new Student("2017-1000", "Marvin", 'M',YearLevel.FIRST, Course.BSIT, College.CCS)
        ));
    }

    static void createTestScan() {
        Stream<Student> studentsStream = null;

        String cc = "";
        String yrlv = "";
        String dep = "";
        for (College d : College.values()) {
            dep += d.name() + ", ";
        }
        dep = dep.substring(0, dep.length() - 2);
        for (Course c : Course.values()) {
            cc += c.name() + ", ";
        }
        cc = cc.substring(0, cc.length() - 2);
        for (YearLevel y : YearLevel.values()) {
            yrlv += y.name() + ", ";
        }
        yrlv = yrlv.substring(0, yrlv.length() - 2);

        // Create a Scanner object
        int choice = 0;
        char sortBy = 'A';
        Scanner scanner = new Scanner(System.in);
        do {
            // Read values from Console
            System.out.println("========Choose option below to start========");
            System.out.println("Enter [0] to set filter students by College: ");
            System.out.println("Enter [1] to set filter students by Course: ");
            System.out.println("Enter [2] to set filter students by Year-level: ");
            System.out.println("Enter [3] to set filter students by Gender: ");
            System.out.println("Enter [4] to sort students (by default it is sorted by student-number): ");
            System.out.println("Enter [5] to show the results: ");
            System.out.println("============================================");
//            try (Scanner scanner = new Scanner(System.in)) {
//            if(!scanner.hasNextInt()) {
//                break;
//            }
            choice = scanner.nextInt();

                switch (choice) {
                    case 0:
                        System.out.println("Enter the value for 'College' of the students (valid values are "+dep+"): ");
                        String college = scanner.next();
                        studentsStream  = students.stream()
                                .filter(s -> s.college == College.valueOf(college));
                        break;
                    case 1:
                        System.out.println("Enter the value for 'Course' of the students (valid values are "+cc+"): ");
                        String course = scanner.next();
                        studentsStream  = students.stream()
                                .filter(s -> s.course == Course.valueOf(course));
                        break;
                    case 2:
                        System.out.println("Enter the value for 'Year-level' of the students (valid values are "+yrlv+"): ");
                        String yearLevel = scanner.next();
                        studentsStream  = students.stream()
                                .filter(s -> s.yearLevel == YearLevel.valueOf(yearLevel));
                        break;
                    case 3:
                        System.out.println("Enter the value for 'Gender' of the students (valid values are M and F): ");
                        char gender = scanner.next(".").charAt(0);
                        studentsStream  = students.stream()
                                .filter(s -> s.gender == gender);
                        break;

                    case 4:
                        System.out.println("Enter [A] to sort by Student-number");
                        System.out.println("Enter [B] to sort by Name");
                        System.out.println("Enter [C] to sort by Gender");
                        System.out.println("Enter [D] to sort by Course");
                        System.out.println("Enter [E] to sort by Year-level");
                        System.out.println("Enter [F] to sort by College");
                        sortBy = scanner.next(".").charAt(0);

                        break;
                    case 5:
                        if(studentsStream == null)
                            studentsStream = students.stream();

                        List<Student> result = studentsStream.collect(Collectors.toList());
                        System.out.println("---------------------------RESULTS---------------------------");
                        if(result.size()==0) {
                            System.out.println("No results found!");
                        } else {
                            if(sortBy == 'A') {
                                Collections.sort(result, Student.COMPARE_BY_NUMBER);
                                System.out.println("Sort by student number");
                            } else if(sortBy == 'B') {
                                Collections.sort(result, Student.COMPARE_BY_NAME);
                                System.out.println("Sort by name");
                            } else if(sortBy == 'C') {
                                System.out.println("Sort by gender");
                                Collections.sort(result, Student.COMPARE_BY_GENDER);
                            } else if(sortBy == 'D') {
                                System.out.println("Sort by course");
                                Collections.sort(result, Student.COMPARE_BY_COURSE);
                            } else if(sortBy == 'E') {
                                System.out.println("Sort by year-level");
                                Collections.sort(result, Student.COMPARE_BY_YEARLEVEL);
                            } else {
                                System.out.println("Sort by college");
                                Collections.sort(result, Student.COMPARE_BY_COLLEGE);
                            }
                            System.out.println("The to total number of students is " + result.size());
                            String format = "%-20s%-10s%-10s%-10s%-10s%-10s%n";
                            System.out.println("-------------------------------------------------------------");
                            System.out.printf(format, "#", "Name", "Gender", "Course", "YrLv", "College");
                            System.out.println("-------------------------------------------------------------");
                            result.forEach(s -> {
                                ;
                                System.out.printf(format, s.number, s.name, s.gender, s.course, s.yearLevel, s.college);
                            });
                        }
                        System.out.println("-------------------------------------------------------------");
                        studentsStream = null;
                        break;
                }
//            } catch (Exception e) {
//                e.printStackTrace(System.err);
//            }
        } while(choice != 5);


    }

    public static void main(String[] args) {
        createTestData();
        createTestScan();
    }
}
